import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'my-app',
  template: `
     <p>Result: {{result}}</p>
     <p>Time: {{time}}</p>
     <p>Error: {{error}}</p>
  `
})
export class AppComponent {

  result;
    constructor(){
       this.countDown(10)
        .subscribe(result => this.result = result, null, () => this.result = 'Complete!');
    }   
    countDown(start):Observable<number> {
      return Observable.timer(1, 1000)
          .map(x => start -x)
          .takeWhile(x => x>0);
    }
  }
  
  /*  constructor(){
    this.countDown(7)
      .subscribe(result => this.result = result, null, () => this.result = 'Complete!');
}
  countDown(start):Observable<number> {
    return Observable.create(observer => {
      let counter = start;
      observer.next(counter --);
      const intervalId = setInterval( () => {
        if(counter >= 0){
          observer.next(counter--);
        }else{
          observer.complete();
          clearInterval(intervalId);
        }
        
    }, 1000); 
    });
  }
}
*/
